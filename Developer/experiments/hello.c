// socket interface
#include <sys/socket.h>
// epoll interface
#include <sys/epoll.h>
// struct sockaddr_in
#include <netinet/in.h>
// IP addr convertion
#include <arpa/inet.h>
// file decriptor controller
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
// malloc() free()
#include <stdlib.h>
#include <errno.h>
// fork
#include <signal.h>

#include <string.h>
#include <sys/types.h>
#include <netdb.h>

#define VERSION     1
#define BUFSIZE  4096
#define ERROR      42
#define LOG        44
#define FORBIDDEN 403
#define NOTFOUND  404
#define MAX_EVENTS 20

//structures

static make_socket_non_blocking(int sfd)
{
    int flags

    if((flags = fcntl(sfd, F_GETFL, 0)) < 0 )
	errexit("GETFL %d failed", sfd);

    flags |= O_NONBLOCK;
    if(fcntl (sfd, F_SETFL, flags) < 0)
	errexit("SETFL %d failed" sfd):
}

/* this is a child web server process, so we can exit on errors */
void web(int fd, int connfd, int hit)
{
    //event loop
    for(;;)
    {
	nfds = epoll_wait(efd, events, MAX_EVENTS, -1);
	if(ndfs = -1)
	{
	    perror("epoll_wait");
	    exit(EXIT_FAILURE);
	}

	for(i = 0; 1 < ndfs, i++)
	{
	    if(events[i].data.fd == fd)
	    {
		if(connfd == -1)
		{
		    //todo
		}
		make_socket_non_blocking(connfd);
		event.data.fd = connfd;
		event.events = EPOLLIN | EPOLLET;
		if(epoll_ctl(efd, EPOLL_CTL_ADD, connfd, &event ) == -1)
		{
		    perror("epoll_ctl: conn_sock");
		    exit(EXIT_FAILURE);
		}
		else
		{
		    module(events[i].data.fd);
		}
	    }
	}
    } 
}

int main(int argc, char **argv)
{
	int i, port, pid, listenfd, socketfd, hit;
	socklen_t length;
	static struct sockaddr_in cli_addr; /* static = initialised to zeros */
	static struct sockaddr_in serv_addr; /* static = initialised to zeros */

	if( argc < 3  || argc > 3 || !strcmp(argv[1], "-?") ) {
		(void)printf("hint: nweb Port-Number Top-Directory\t\tversion %d\n\n"
	"\tnweb is a small and very safe mini web server\n"
	"\tnweb only servers out file/web pages with extensions named below\n"
	"\t and only from the named directory or its sub-directories.\n"
	"\tThere is no fancy features = safe and secure.\n\n"
	"\tExample: nweb 8181 /home/nwebdir &\n\n"
	"\tOnly Supports:", VERSION);
		for(i=0;extensions[i].ext != 0;i++)
			(void)printf(" %s",extensions[i].ext);

		(void)printf("\n\tNot Supported: URLs including \"..\", Java, Javascript, CGI\n"
	"\tNot Supported: directories / /etc /bin /lib /tmp /usr /dev /sbin \n"
	"\tNo warranty given or implied\n\tNigel Griffiths nag@uk.ibm.com\n"  );
		exit(0);
	}
	if( !strncmp(argv[2],"/"   ,2 ) || !strncmp(argv[2],"/etc", 5 ) ||
	    !strncmp(argv[2],"/bin",5 ) || !strncmp(argv[2],"/lib", 5 ) ||
	    !strncmp(argv[2],"/tmp",5 ) || !strncmp(argv[2],"/usr", 5 ) ||
	    !strncmp(argv[2],"/dev",5 ) || !strncmp(argv[2],"/sbin",6) ){
		(void)printf("ERROR: Bad top directory %s, see nweb -?\n",argv[2]);
		exit(3);
	}
	if(chdir(argv[2]) == -1){ 
		(void)printf("ERROR: Can't Change to directory %s\n",argv[2]);
		exit(4);
	}
	/* Become deamon + unstopable and no zombies children (= no wait()) */
	if(fork() != 0)
		return 0; /* parent returns OK to shell */
	(void)signal(SIGCLD, SIG_IGN); /* ignore child death */
	(void)signal(SIGHUP, SIG_IGN); /* ignore terminal hangups */
	//for(i=0;i<32;i++)
	//	(void)close(i);		/* close open files */
	(void)setpgrp();		/* break away from process group */
	logger(LOG,"nweb starting",argv[1],getpid());
	/* setup the network socket */
	if((listenfd = socket(AF_INET, SOCK_STREAM,0)) <0)
		logger(ERROR, "system call","socket",0);
	port = atoi(argv[1]);
	if(port < 0 || port >60000)
		logger(ERROR,"Invalid port number (try 1->60000)",argv[1],0);
        char *aip;
        uint32_t ip;
        aip = getenv("OPENSHIFT_DIY_IP"); //Openshift specific, replace with IP
        inet_pton(AF_INET, aip, &(serv_addr.sin_addr));
        printf("IP as string: %s\nIP as integer: %d\n", aip, serv_addr.sin_addr);
	serv_addr.sin_family = AF_INET;
	//serv_addr.sin_addr.s_addr = htonl(ip);//(INADDR_ANY);
	serv_addr.sin_port = htons(port);
	if(bind(listenfd, (struct sockaddr *)&serv_addr,sizeof(serv_addr)) <0)
		logger(ERROR,"system call","bind",0);
	if( listen(listenfd,64) <0)
		logger(ERROR,"system call","listen",0);
	for(hit=1; ;hit++) {
		length = sizeof(cli_addr);
		if((socketfd = accept(listenfd, (struct sockaddr *)&cli_addr, &length)) < 0)
			logger(ERROR,"system call","accept",0);
		if((pid = fork()) < 0) {
		    	logger(ERROR,"system call","fork",0);
		}
		else {
			if(pid == 0) { 	/* child */
				(void)close(listenfd);
				web(socketfd,hit); /* never returns */
			} else { 	/* parent */
				(void)close(socketfd);
			}
		}
	}
}
