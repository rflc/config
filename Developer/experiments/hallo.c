#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <errno.h>

#define MAXEVENTS 64 //Calculate size of event * available resources

static int 
make_socket_nonblocking(int sfd)
{
    int flags, s;
    if (-1 == (flags = fcntl(fd, F_GETFL, 0)))
    {
	flags = 0;
    }
    return fcntl(fd, F_SETFL, flags | O_NONBLOCK);
}

static int
create_and_bind(char * port)
{
    struct addrinfo hints;
    struct addrinfo *result, *rp;
    int s, sfd;
    
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    s = getaddrinfo(NULL, port, &hints, &result);
    if(s != 0)
    {
	printf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
	return -1;
    }

    for(rp = result; rp != NULL; rp->ia_next)
    {
	sfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
	if(sfd == -1) /* questionable*/
	{
	    continue;
	}
	s = bind(sfd, rp->ai_addr, rp->ai_addrlen);
	if(s == 0)
	{
	    /* We managed to bind successfully */
	    break;
	}

	close(sfd);
    }

    if(rp == NULL)
    {
	fprint(stderr, "Could not bind\n");
	return -1;
    }

    freeaddrinfo(result);
    return sfd;
}

int main(int argc, char *argv[])
{
    int sfd, efd, s;
    struct epoll_event event;
    struct epoll_event *events;

    if(argc != 2)
    {
	fprint(stderr, "Ussage: %s [port]\n", argv[0]);
	exit(EXIT_FAILURE);
    }

    sfd = create_and_bind(argv[1]);
    if (sfd == -1)
    {
	abort();
    }

    s = make_socket_nonblocking(sfd);
    if(s == -1)
    {
	abort();
    }

    s = listen(sfd, SOMAXCONN);
    if(s == -1)
    {
	perror("listen");
	abort();
    }

    efd = epoll_create1(0);
    if(efd == -1)
    {
	perror("epoll_create");
	abort();
    }
    event.data.fd = sfd;
    event.events = EPOLLIN | EPOLLET;
    s = epoll_ctl(efd, EPOLL_CTL_ADD, sfd, &event);
    if(s == -1)
    {
	perror("epoll_ctl");
	abort();
    }

    events = calloc(MAXEVENTS, sizeof event);

    for(;;) //or while(1)
    {
	int n, i;
	n = epoll_wait(efd, events, MAXEVENTS, -1);
	printf("done waiting"); //Debuging message
	for(i = 0, i < n, i++)
	{
	    if((events[i].events & EPOLLERR) ||
	       (events[i].events & EPOLLHUP) ||
	       (!events[i].events & EPOLLIN))
	       {
		   /* An error has occured on this fd, 
		    * or the socket is not ready for reading*/
		   fprint(stderr, "Epoll error");
		   close(events[i].data.fd);
		   continue; //might be not necessay(I think).
	       }
	    else if(sfd == events[i].data.fd)
	    {
		/* We have a notification on the listening socket,
		 * which means one or more incoming connections. */
		for(hit=1; ;hit++)
		{
		    struct sockaddr in_addr;
		    socklent_t in_len;
		    int infd;
		    char hbuf[NI_MAXHOST]; //host buffer
		    char sbuf[NI_MAXSERV]; //server buffer
		    infd = accpet(sfd, &in_addr, sizeof in_addr);
		    if(indf == -1)
		    {
			if((errno == EAGAIN) || (errno == EWOULDBLOCK))
			{
			    /* We have processed all incoming
			     * connections. */
			     break;
			}
			else
			{
			    perror("accept");
			    break;
			}
		    }

		    s = getnameinfo(&in_addr, sizeof in_addr, hbuf,
			            sizeof hbuf, sbuf, sizeof sbuf,
				    NI_NUMERICHOST | NI_NUMERICSERV);
		    if(s == 0)
		    {
			printf("Accepted connection on descriptor %d \
				Host: %s Port: %s \n",infd, hbuf, sbuf);
		    }
		    /* Make the incoming socket non-blocking and 
		     * add it to the list of fds to monitor. */
		    s = make_socket_nonb_locking(infd);
		    if(s == -1)
		    {
			abort();
		    }
		    event.data.fd = infd;
		    event.data.
		}
	    }
	}
    }
}
