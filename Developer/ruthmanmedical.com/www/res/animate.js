const mq = window.matchMedia('(min-width: 500px)');
if (mq.matches) {
// window width is at least 500px
//var pc = document.getElementById('pc').getBoundingClientRect();

var pcs = document.getElementById('pcs'); //CHANGE THIS TO WHOLE SECTION
var vh = window.innerHeight;
var tmx = document.getElementById('tm');

var dirty = true;
var tl = new TimelineMax({paused: true});
tl.to('#hr', 4, {rotation:360, svgOrigin:"-128.54px -145.643px"/*"151.46px47.357px"*/, 
    ease:Linear.easeNone, delay:0.6})
  .to('#mn', 2, {rotation:360, svgOrigin:"-128.54px -145.643px", 
    ease:Linear.easeNone, repeat:1, delay:0.6}, 0);

document.addEventListener("scroll", update);
document.addEventListener("touchmove", queueUpdate);  
//TweenMax.ticker.addEventListener("tick", update);

//TweenLitie.to('#eiffel', 16, {y:"220px"});

function queueUpdate() {
    dirty = true;
}

function update() {
var pc = pcs.getBoundingClientRect();
var tm = tmx.getBoundingClientRect();
console.log("update called");
console.log(pc.top);
console.log(pc.bottom);
if (tm.bottom <= 0 && pc.top >= 0 && pc.bottom <= vh) {
    if (!tl.isActive()){
    console.log("Animation Runs..");
    tl.restart();
}
}
else tl.stop();
}
}

else {
// window width is less than 500px
    alert("Hello there");
}
