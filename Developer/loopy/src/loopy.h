// Exposes main definitions to modules
#ifndef LOOP_H
#define LOOP_H
// pthreads
#include <pthread.h>
#define BUFSIZE  4096
#define RSCSIZE  32
#define CACHE    32768
#define NBLOCKS  64
#define BLOCK    512
#define LINE     64


// 32 byte structure
typedef struct Member
{
    pthread_t        tid;   //8-15  (8)
    unsigned    received;   //16-19 (4)
    unsigned     written;
    int             nfds;   //20-23 (4)
    int               fd;   //24-27 (4)
    int           connfd;   //28-31 (4)
//    int      tstat : 1; 36-..
//    int      hstat : 1;
    // environment variables
    char  request[BUFSIZE];
    short request_method; //GET HEAD PUT POST
    char  request_resource[RSCSIZE];
    char  * query_string;
    int   content_type;
    int   rfd; //resource fd
    long   content_lenght;
    int   content_encoding;
    char  response[BUFSIZE];
    char * rptr;
} Member;

// thread poll
typedef struct Threads
{
    unsigned count;
    Member *thread;
} Threads;

#endif
