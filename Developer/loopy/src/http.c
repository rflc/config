#include <stdio.h> // remove
#include <time.h>
#include <string.h>
#include "http.h"
struct
{
    char *ext;
    char *mimetype;
} extensions [] = 
{
    {"gif",  "image/gif"      	     },
    {"png",  "image/png"      	     },
    {"jpg",  "image/jpg"      	     },
    {"jpeg", "image/jpeg"            },
    {"html", "text/html"             },
    {"htm",  "text/html"             },
    {"css",  "text/css"              },
    {"js",   "application/javascript"},
    {"ico",  "image/ico"             },
    {"zip",  "image/zip"             },
    {"tar",  "image/tar"             },
    {"gz",   "image/gz"              },
    {"txt",  "text/plain"            },
    {0,0}
};

// controls http_header()
void http_response(Member *t)
{
    if((t->rfd = open(t->request_resource, O_RDONLY)) == -1)
	http_header(NOTFOUND);
	t->content_length = (long) lseek(t->rfd, (off_t) 0, SEEK_END);
    lseek(t->rfd, (off_t) 0, SEEK_SET);
    //return content_lenght for header
    //copy file to response
}

void http_parse(Member *t)
{
    int i;
    int j = 0;

    if (strncmp(t->request, "GET ", 4) == 0) //replicate for POST etc
    {
	t->request_method = 0;
	t->request_method |= GET;
	for(i=5; i < BUFSIZE; i++)
	{
	    // RSCSIZE overflow check not implemented 
	    if(t->request[i] == ' ')
	    {
		if (t->request_resource[--j] == '/')
		{
		    t->request_resource = "index.html";
		    break;
		}
		t->request_resource[j] = '\0';
		break;
	    }
	    t->request_resource[j] = t->request[i];
	    j++;
	}
	printf("%s\n", t->request_resource);
    }
    else if (strncmp(t->request, "POST ", 5) == 0)
	t->request_method |= POST;
}

void http_date(Member *t)
{
    //adjust to possible max value
    time_t now;
    struct tm tm;
    time(&now);
    gmtime_r(&now, &tm);
    /*
    memcpy(t->rptr, "HTTP/1.1 200 OK\n"
	    "content-type: text/html; charset=UTF-8\n" 
	    "Date: ", 61);
    */
    t->rptr += 
    strftime(t->rptr, sizeof BUFSIZE, "Date: %a, %d %b %Y %H:%M:%S %Z\r\n", &tm);
}

void http_mime(Member *t)
{
    int i, len;

    for(i=0; extensions[i].ext != 0; i++)
    {
	len = strlen(extensions[i].ext);
	if (!strncmp(t->request[BUFSIZE - len], extensions[i].ext, len))
	{
	    sprintf(t->rptr, "Content-Type: %s\r\n", extensions[i].mimetype);
	    break;
	}
    }
}


void http_header(Member *t, short type)
{

    switch (type)
    {
	case OK:
	     strcpy(t->response, "HTTP/1.1 200 OK\r\n"); // HTTP 1.1 ...
	     t->rptr = t->response + 17;
	     http_date(&t);                              // Date:
	     http_mime(&t);                              // Content-Type:
	     http_resource(&t);                          // Content-Length: + body
	     break;
	case NOTFOUND:
	     strcpy(t->response, "HTTP/1.1 404 Not Found\r\n\r\n");
	     break;
	case FORBIDDEN:
	     strcpy(t->response, "HTTP/1.1 403 Forbidden\r\n\r\n");
	     break;
    }
    printf(t->response);
}
