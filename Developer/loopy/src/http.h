// exposes http module definitions
#ifndef HTTP_H
#define HTTP_H

#include "loopy.h"

enum
{
    OK        = 1 << 0,
    FORBIDDEN = 1 << 1,
    NOTFOUND  = 1 << 2
};

enum
{
    GET  = 1 << 0,
    HEAD = 1 << 1,
    PUT  = 1 << 2,
    POST = 1 << 3 
};

void http_parse(Member *);
void http_response(void);
void http_header(Member *, short);

#endif
