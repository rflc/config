#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>

void conf(struct sockaddr_in *serveraddr)
{
    DIR *dir;
    char *ip;
    char *port;
    char *protocol; // unused
    static struct dirent entry;
    static struct dirent *pentry;

    if(!(dir = opendir(".")))
	exit(EXIT_FAILURE);
    while((readdir_r(dir, &entry, &pentry) != NULL == 0) && pentry != NULL)
    {
	if(entry.d_type != DT_DIR)
	    continue;
	if(strcmp(entry.d_name, "..") == 0 || 
	   strcmp(entry.d_name,  ".") == 0)
	    continue;
	printf("Dir: %s\n", entry.d_name);
	/*
	//printf("prev: %s\n", entry.d_name);
	ip = strdup(entry.d_name);
	ip = strsep(&ip, ":");
	if(inet_pton(AF_INET, ip, &(serveraddr)->sin_addr) != 1)
	    exit(EXIT_FAILURE);
	port = strdup(entry.d_name);
	port += (strlen(ip) + 1);
	port = strsep(&port, "_");
	if(!(serveraddr->sin_port = htons(atoi(port))))
	    exit(EXIT_FAILURE);
	protocol = entry.d_name + strlen(port) + strlen(ip) + 2;
	printf("IP: %s\nPort: %s\nProtocol: %s\n", ip, port, protocol);
	//return;
	*/
    }
}

int main(int argc, char **argv)
{
    static struct sockaddr_in serveraddr;
    conf(&serveraddr);
    //printf("%s\n", serveraddr.sin_addr);
    return 0;
}
