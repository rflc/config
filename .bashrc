# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific aliases and functions

#use -p 433 to reroute default ssh port 22 to 433 (library)
#alias ide='ssh -i /Volumes/keychain/ide.pem r@162.243.57.77 -p 443'
alias ide='ssh -i /Volumes/keychain/ide.pem r@ec2-52-91-224-193.compute-1.amazonaws.com'

# usage: t project file
#t() {
#scp -i /Volumes/keychain/ide.pem /Users/rflcbn/Documents/Developer/$2/$1.* r@162.243.57.77:developer/$2/res
#}

# usage: t filename dest (ssh host) path
u() {
    scp -r $1 $2:~/$3
}

# usage: d path/src path/dest flag (use uppercase -P to change port for scp)
d(){
scp $3 prod:$1 $2
}
