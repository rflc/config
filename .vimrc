" rflc's vimrc

" This must be first.
set nocompatible

"Character encoding (Alt key)
set fileencodings=utf-8


"Backup
"set nobackup

set backupdir=~/.vim/backup//
set directory=~/.vim/swap//
"set undodir=~/.vim/undo// "Broken

"Fix meta-keys which generate <Esc>a .. <Esc>z (Alt on PuTTY)
"let c='a'
"while c <= 'z'
"  exec "set <M-".toupper(c).">=\e".c
"  exec "imap \e".c." <M-".toupper(c).">"
"  let c = nr2char(1+char2nr(c))
"endw

"allow backspacing over everything in insert mode
set backspace=indent,eol,start

if has("vms")
set nobackup	" do not keep a backup file, use versions instead
else
set backup	" keep a backup file
endif

set history=50	" keep 50 lines of command line history
set ruler   " show the cursor position all the time
set showcmd   " display incomplete commands
set incsearch   " do incremental searching

" For Win32 GUI: remove 't' flag from 'guioptions': no tearoff menu entries
" let &guioptions = substitute(&guioptions, "t", "", "g")

" Don't use Ex mode, use Q for formatting
map Q gq

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
inoremap <C-U> <C-G>u<C-U>

" In many terminal emulators the mouse works just fine, thus enable it.

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""set mouse=a

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
syntax on
set hlsearch

" Color
"set term=xterm-256color
set t_Co=256
colorscheme kolor
"highlight Normal ctermbg=NONE
"highlight nonText ctermbg=NONE

" Only do this part when compiled with support for autocommands.
if has("autocmd")

" Enable file type detection.
" Use the default filetype settings, so that mail gets 'tw' set to 72,
" 'cindent' is on in C files, etc.
" Also load indent files, to automatically do language-dependent indenting.
filetype plugin indent on

" Put these in an autocmd group, so that we can delete them easily.
augroup vimrcEx
au!

" For all text files set 'textwidth' to 78 characters.
autocmd FileType text setlocal textwidth=78

" When editing a file, always jump to the last known cursor position.
" Don't do it when the position is invalid or when inside an event handler
" (happens when dropping a file on gvim).
" Also don't do it when the mark is in the first line, that is the default
" position when opening a file.
autocmd BufReadPost *
\ if line("'\"") > 1 && line("'\"") <= line("$") |
\   exe "normal! g`\"" |
\ endif

augroup END

"Display opened filename in Tmux's statusbar
autocmd BufReadPost,FileReadPost,BufNewFile * call system("tmux rename-window %")

"Indentation spacing
set shiftwidth=4 

set autoindent	" always set autoindenting on

endif " has("autocmd")

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
command DiffOrig vert new | set bt=nofile | r # | 0d_ | diffthis
\ | wincmd p | diffthis
endif

"Numbered lines
set number

"Abbrevations

iabbrev @@ rafael@cloudstom.com
iabbrev cm http://cloudstom.com

"Execute Make deploy on file write @ ~/developer/*
"au BufWritePost ~/developer/*/*.html !make deploy
"au BufWritePost ~/developer/*/res/*.* !make deploy

"Map <esc> to <caps lock>

inoremap <F12> <esc>
vnoremap <F12> <esc>
nnoremap <F12> <esc>:

"Move to begining and end of a line

exec "set <M-h>=\eh"
exec "set <M-l>=\el"

nnoremap <M-h> 0
nnoremap <M-l> $
inoremap <M-h> 0
inoremap <M-l> $

"Move lines up and down with left-Alt + h(up) or j(down)

exec "set <M-j>=\ej"
exec "set <M-k>=\ek"

nnoremap <M-j> :m+<CR>==
nnoremap <M-k> :m-2<CR>==
inoremap <M-j> <Esc>:m+<CR>==gi
inoremap <M-k> <Esc>:m-2<CR>==gi
vnoremap <M-j> :m'>+<CR>gv=gv
vnoremap <M-k> :m-2<CR>gv=gv

"Opem fold with the space bar
nnoremap <space> za

"<Ctrl-l> redraws the screen and removes any search highlighting.
nnoremap <silent> <C-l> :nohl<CR><C-l>

"Saves and restores last view
au BufWinLeave ?* mkview
au BufWinEnter ?* silent loadview

"Fold marker.
set foldmethod=marker

"Mark end of operator target with $
set cpoptions+=$

"Tmux autocommands
"display filename in status bar
"autocmd BufReadPost,FileReadPost,BufNewFile * call system("tmux rename-window " . expand("%"))

"Autocomplete braces, parenthesis and brackets.
inoremap {  	{}<Left>

inoremap {<CR>  {<CR>}<Esc>O

inoremap {{ 	{

inoremap {} 	{}

inoremap (  	()<Left>

inoremap (<CR>  (<CR>)<Esc>O

inoremap (( 	(

inoremap () 	()

inoremap [  	[]<Left>

inoremap [<CR>  [<CR>]<Esc>O

inoremap [[ 	[

noremap [] 	[]

"Autocommands
":autocmd autocmd-events filter command

"Dissabled keys

cnoremap <F12>  <nop>
noremap <right> <nop>
noremap <left>  <nop>
noremap <up>    <nop>
noremap <down>  <nop>

"Cscope

function! LoadCscope()
      let db = findfile("cscope.out", ".;")
        if (!empty(db))
	   let path = strpart(db, 0, match(db, "/cscope.out$"))
           set nocscopeverbose " suppress 'duplicate connection' error
           exe "cs add " . db . " " . path
	   set cscopeverbose
	endif
endfunction
au BufEnter /* call LoadCscope()
