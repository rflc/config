# .bash_profile


# Get the aliases and functions

if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi


# Set 246 Colors

if [ "$TERM" == "xterm" ]; then
      export TERM='xterm-256color'
fi


# User specific environment and startup programs

export PS1='\[\e[38;5;239m\][\W]\>\[\e[0m\] '

PATH=$PATH:$HOME/bin
PATH+= /usr/local/opt/emscripten/emsdk
PATH+= /usr/local/opt/emscripten/emsdk/clang/e1.38.1_64bit
PATH+= /usr/local/opt/emscripten/emsdk/node/8.9.1_64bit/bin
PATH+= /usr/local/opt/emscripten/emsdk/emscripten/1.38.1

export PATH

. /usr/local/opt/emscripten/emsdk/emsdk_env.sh
clear

# Attach tmux session on login

if [ ! -n "$TMUX" ]; then
    tmux a
fi


# Aliases

alias ide='ssh ide'
alias yum='sudo yum'


# Functions

# dev name ip port domain
dev() {
#    if [ $1 == "www" ]; then
	mkdir -p ~/Developer/$1/$2:$3/www
	cp ~/Developer/.res/Makefile ~/Developer/$1/Makefile
	cp ~/Developer/.res/index.html ~/Developer/$1/$2:$3/www/index.html
#    elif [ $1 == "c" ]; then
#	: # no-op command (do nothing)
	# do c stuff
	# copy CMakefile 
#    else
	mkdir -p ~/Developer/$1
	cp ~/Developer/.res/Makefile ~/Developer/$1/Makefile
#fi
}
